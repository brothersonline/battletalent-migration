import React from 'react';
import DefaultView from "./Views/DefaultView";
// import ReactGA from "react-ga4";

// const trackingId = "G-9GQJ8VLVD5";
// ReactGA.initialize(trackingId);

function App() {
  // ReactGA.pageview(window.location.pathname + window.location.search);

  return (
    <div className="App">
      <DefaultView />
    </div>
  );
}

export default App;
