import React, {useEffect, useState} from 'react';
import GamesList from "../Components/GamesList";
import { get } from "axios";

const versionTag = "0.0.9";
// const versionTag = "0.0.7";
const brokenVersionTag = "Broken on 0.0.9";
const gripBuggedTag = "Grip bugged on 0.0.9";

function DefaultView() {
  const [unknownMods, setUnknownMods] = useState(null);
  const [supportedMods, setSupportedMods] = useState([]);
  const [unsupportedMods, setUnsupportedMods] = useState([]);
  const [gripBuggedMods, setGripBuggedMods] = useState([]);
  
  const gameId = '2340';
  const apiKey = '75ff43bf10298fc51dce4cd26368b39c';

  useEffect(() => {
    async function fetchUnknownMods() {
      const newUrl = `https://api.mod.io/v1/games/${gameId}/mods?api_key=${apiKey}&tags-not-in=${versionTag},${brokenVersionTag},${gripBuggedTag}`;
      const {data} = await get(newUrl).then(resp => {
        return resp;
      });
      setUnknownMods(data);
    }
    async function fetchSupportedMods() {
      const newUrl = `https://api.mod.io/v1/games/${gameId}/mods?api_key=${apiKey}&tags=${versionTag}`;
      const {data} = await get(newUrl).then(resp => {
        return resp;
      });
      setSupportedMods(data);
    }
    async function fetchUnsupportedMods() {
      const newUrl = `https://api.mod.io/v1/games/${gameId}/mods?api_key=${apiKey}&tags=${brokenVersionTag}`;
      const {data} = await get(newUrl).then(resp => {
        return resp;
      });
      setUnsupportedMods(data);
    }
    async function fetchGripBuggedMods() {
      const newUrl = `https://api.mod.io/v1/games/${gameId}/mods?api_key=${apiKey}&tags=${gripBuggedTag}`;
      const {data} = await get(newUrl).then(resp => {
        return resp;
      });
      setGripBuggedMods(data);
    }

    fetchUnknownMods();
    fetchSupportedMods();
    fetchUnsupportedMods();
    fetchGripBuggedMods();
  }, []);

  if(!unsupportedMods || !supportedMods || !unknownMods || !gripBuggedMods){
    return <p>Loading</p>;
  }

  return (
    <>
      <header id="home">
        <section className="hero">
          <div className="container">
            <div className="row text-center animated fadeInUp hero-button-row">
              <div className="col-md-12 wp4">
              </div>
            </div>
          </div>
        </section>
      </header>
      <section className="text-center section-padding">
        <h1>Mods to check ({unknownMods.result_total})</h1>
        <p>Note: List shows a max of 100 items</p>
        {unknownMods && <GamesList mods={unknownMods.data} status={"unknown"}/>}
        <h1>{versionTag} ({supportedMods.result_total})</h1>
        <a href={"https://battletalent.mod.io/?filter=t&tag%5B%5D=0.0.9"}>View supported mods here</a>
        {/*{supportedMods && <GamesList mods={supportedMods.data} status={"supported"}/>}*/}
        <h1>{brokenVersionTag} ({unsupportedMods.result_total})</h1>
        <a href={"https://battletalent.mod.io/?filter=t&tag%5B%5D=Broken+on+0.0.9"}>View broken mods here</a>
        {/*{unsupportedMods && <GamesList mods={unsupportedMods.data} status={"unsupported"}/>}*/}
        <h1>{gripBuggedTag} ({gripBuggedMods.result_total})</h1>
        <a href={"https://battletalent.mod.io/?filter=t&tag%5B%5D=Grip+bugged+on+0.0.9"}>View grip bugged mods here</a>
        {/*{gripBuggedMods && <GamesList mods={gripBuggedMods.data} status={"unsupported"}/>}*/}
      </section>
    </>
  );
}

export default DefaultView;
