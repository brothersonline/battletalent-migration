import React from 'react';
import "./GamesList.css";

function GamesList({mods = [], status = "works"}) {
  function getIconClass() {
    if (status === "unsupported") {
      return 'fa-times-circle';
    }
    if(status === "supported"){
      return 'fa-check-circle'
    }

    return 'fa-question-circle';
  }

  function getIconColor() {
    if (status === "unsupported") {
      return 'red';
    }
    if(status === "supported"){
      return 'green'
    }

    return '#ffc200';
  }

  return (
    <div className="container">
      {mods.length === 0 &&
      <div>No mods where found</div>
      }
      <div className="row text-center animated fadeInUp">
        <div className="col-md-12 wp4 mods-list">
          {mods.map((mod, i) => (
              <div className={"class-row"} key={mod.name}>
                <div className={"class-row-character-name"}>
                  <h1><i className={`far ${getIconClass()}`} style={{color: getIconColor()}}/> {mod.name}</h1>
                  <a href={mod.profile_url}>{mod.profile_url}</a>
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
}

export default GamesList;
